public enum Tile {
	BLANK("_"),
	X("X"),
	O("O");
	
	//FIELDS
	private final String name;
	
	//CONSTRUCTOR
	private Tile(String name) {
		this.name = name;
	}
	
	//GET METHOD
	public String getName() {
		return this.name;
	}
}