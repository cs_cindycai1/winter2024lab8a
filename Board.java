public class Board {
	//FIELDS
	private Tile[][] grid;
	
	//CONSTRUCTOR
	public Board() {
		int size = 3;
		
		grid = new Tile[size][size];
		
		for (int row = 0; row < grid.length; row++) { //for each row
			for (int col = 0; col < grid[row].length; col++) { //for each column
				grid[row][col] = Tile.BLANK;
			}
		}
	}
	
	//TOSTRING
	public String toString() {
		String result = "";
		
		for (int i = 0; i < this.grid.length; i++) {
			for (int j = 0; j < this.grid[i].length; j++) {
				result += this.grid[i][j].getName() + " ";
			}
			result += "\n";
		}
		
		return result;
	}
	
	//CUSTOM METHOD
	
	//check if place is blank and place player tokens
	public boolean placeToken(int row, int col, Tile playerToken) {
		if (row < this.grid.length && row >= 0 && col < this.grid.length && col >= 0) { //validate row and col are within range
			if (this.grid[row][col] == Tile.BLANK) { //check if position is occupied
				this.grid[row][col] = playerToken;
				return true;
			}
		}
		
		return false;
	}
	
	//check if board has no Tile.BLANK value
	public boolean checkIfFull() {
		for (Tile[] row: this.grid) {
			for (Tile col: row) {
				if (col == Tile.BLANK) {
					return false;
				}
			}
		}
		
		return true;
	}
	
	//check if player has a winning row
	private boolean checkIfWinningHorizontal(Tile playerToken) {
		for (int i = 0; i < this.grid.length; i++) { //for each row
			int counter = 0;
			for (int j = 0; j < this.grid[i].length; j++) { //check if every value in row is playerToken
				if (this.grid[i][j] == playerToken) {
					counter++;
				}
			}
			if (counter == this.grid[i].length) {
				return true; //return true if every space in row is playerToken
			}
		}
		
		return false;
	}
	
	//check if player has a winning column
	private boolean checkIfWinningfVertical(Tile playerToken) {
		for (int col = 0; col < this.grid.length; col++) { //for ever column
			int counter = 0;
			for (int row = 0; row < this.grid.length; row++) { //check if every row in column has playerToken
				if (this.grid[row][col] == playerToken) {
					counter++;
				}
			}
			if (counter == this.grid.length) {
				return true;
			}
		}
		
		return false;
	}
	
	//check if player has winning streak
	public boolean checkIfWinning(Tile playerToken) {
		if (checkIfWinningHorizontal(playerToken) || checkIfWinningfVertical(playerToken) || checkIfWinningDiagonal(playerToken)) {
			return true;
		}
		
		return false;
	}
	
	//BONUS: check if player has winning diagonal
	//could separate into 2 methods
	private boolean checkIfWinningDiagonal(Tile playerToken) {
		int counter = 0;
		for (int i = 0; i < this.grid.length; i++) {
			if (this.grid[i][i] == playerToken) {
				counter++;
			}
		}
		
		if (counter == this.grid.length) {
			return true;
		}
		
		counter = 0;
		int col = this.grid.length - 1;
		for (int row = 0; row < this.grid.length; row++) {
			if (this.grid[row][col] == playerToken) {
				counter++;
			} 
			col--;
		}
		if (counter == this.grid.length) {
			return true;
		}
		
		return false;
	}
}