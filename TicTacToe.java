import java.util.Scanner;

public class TicTacToe {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		System.out.println("Welcome to the Tic-Tac-Toe Game!");
		
		int player1 = 0;
		int player2 = 0;
		
		boolean play = true;
		
		while (play) {
			if (playGame() == 1) {
				player1++;
			} else {
				player2++;
			}
			System.out.println("Press q to stop playing");
			String quit = reader.next();
			if (quit.equals("q")) {
				play = false;
			}
		}
		
		System.out.println("Player 1: " + player1 + "p");
		System.out.println("Player 2: " + player2 + "p");
	}
	
	public static int[] askTokenPlace() {
		Scanner reader = new Scanner(System.in);
		
		int[] position = new int[2];
		System.out.print("Input row: ");
		position[0] = reader.nextInt();
		System.out.print("Input column: ");
		position[1] = reader.nextInt();
		
		return position;
	}
	
	public static int playGame() {
		Board board = new Board();
		
		boolean gameOver = false;
		int player = 1;
		Tile playerT = Tile.X;
		
		while (!gameOver) {
			System.out.println(board);
			
			int[] position = askTokenPlace();
			
			if (player == 1) {
				playerT = Tile.X;
			} else {
				playerT = Tile.O;
			}
			
			while (!board.placeToken(position[0], position[1], playerT)) {
				position = askTokenPlace();
			}
			
			if (board.checkIfWinning(playerT)) {
				System.out.println(board);
				System.out.println("Player " + player + " is the winner!");
				gameOver = true;
			} else if (board.checkIfFull()) {
				System.out.println(board);
				System.out.println("It's a tie!");
				gameOver = true;
			} else {
				player++;
				if (player > 2) {
					player = 1;
				}
			}
		}
		
		return player;
	}
}